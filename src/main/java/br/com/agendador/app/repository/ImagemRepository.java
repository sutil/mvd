package br.com.agendador.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agendador.app.model.entity.Imagem;
import br.com.agendador.repository.ListQueryDslPredicateExecutor;

public interface ImagemRepository extends JpaRepository<Imagem, Long>, ListQueryDslPredicateExecutor<Imagem>
, ImagemRepositoryCustom{

}
