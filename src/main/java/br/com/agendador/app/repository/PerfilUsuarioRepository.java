package br.com.agendador.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agendador.app.login.seguranca.PerfilUsuario;
import br.com.agendador.repository.ListQueryDslPredicateExecutor;

public interface PerfilUsuarioRepository extends JpaRepository<PerfilUsuario, Long>, ListQueryDslPredicateExecutor<PerfilUsuario> {

}
