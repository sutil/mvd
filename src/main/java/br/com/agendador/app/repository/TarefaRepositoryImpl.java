package br.com.agendador.app.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.quartz.SchedulerException;
import org.springframework.transaction.annotation.Transactional;

import br.com.agendador.app.model.entity.Tarefa;

public class TarefaRepositoryImpl implements TarefaRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public Tarefa salvar(Tarefa tarefa) throws SchedulerException {
		boolean ativa = tarefa.isAtiva();
		System.out.println("está ativa? "+ tarefa.isAtiva());
		Tarefa saved = entityManager.merge(tarefa);
		
		if(ativa){
			tarefa.unScheduler();
			tarefa.scheduler(this);
		}
		System.out.println("saved ativa? "+saved.isAtiva());
		System.out.println("tarefa ativa? "+tarefa.isAtiva());
		return saved;
	}

	@Override
	public void deletar(Tarefa tarefa) throws SchedulerException {
		tarefa.unScheduler();
		entityManager.remove(tarefa);
		
	}

	@Override
	public void changeStatus(Tarefa tarefa) throws SchedulerException {
		tarefa.changeStatus();
		salvar(tarefa);
	}

}
