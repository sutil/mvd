package br.com.agendador.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agendador.app.model.entity.Album;
import br.com.agendador.repository.ListQueryDslPredicateExecutor;

public interface AlbumRepository extends JpaRepository<Album, Long>, ListQueryDslPredicateExecutor<Album>{

}
