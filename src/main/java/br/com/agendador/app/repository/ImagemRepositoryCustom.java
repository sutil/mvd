package br.com.agendador.app.repository;

import java.io.IOException;

import br.com.agendador.app.model.entity.Imagem;

public interface ImagemRepositoryCustom {
	
	public Long getUltimoId();
	
	public Imagem salvar(Imagem imagem, String folder) throws IOException;

}
