package br.com.agendador.app.repository;

import static br.com.agendador.app.model.entity.QImagem.imagem;

import java.io.File;
import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.io.FileUtils;
import org.primefaces.model.UploadedFile;

import br.com.agendador.app.model.entity.Imagem;

import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.expr.NumberExpression;

	public class ImagemRepositoryImpl implements ImagemRepositoryCustom {
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Long getUltimoId() {
		JPAQuery query = new JPAQuery(entityManager);
		NumberExpression<Long> max = imagem.id.max();
		return query.uniqueResult(max);
	}
	
	@Override
	public Imagem salvar(Imagem imagem, String folder) throws IOException {
		return gravarImagem(imagem, folder);
	}
	
	private Imagem gravarImagem(Imagem imagem, String folder) throws IOException{
		UploadedFile file = imagem.getFile();
		
		criaDiretorio();
		
		Imagem saved = entityManager.merge(imagem);
		Long id = saved.getId();
		
		String name = String.valueOf(id);
		
		String path = folder+"/"+name+getTipo(file);
		
		File destino = new File(path);
		FileUtils.copyInputStreamToFile(file.getInputstream(), destino);
		
		saved.setUrl(path);
		entityManager.merge(saved);
		return saved;
	}
	
	private void criaDiretorio() {
		ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
		String path = ctx.getRealPath("fotos");
		File folder = new File(path);
		if(!folder.isDirectory()){
			folder.mkdirs();
		}
	}
	
	private String getTipo(UploadedFile file){
		String name = file.getFileName();
		return name.substring(name.lastIndexOf('.'), name.length());
	}

}
