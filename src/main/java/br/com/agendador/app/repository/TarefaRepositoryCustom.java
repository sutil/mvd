package br.com.agendador.app.repository;

import org.quartz.SchedulerException;

import br.com.agendador.app.model.entity.Tarefa;

public interface TarefaRepositoryCustom {
	
	public Tarefa salvar(Tarefa tarefa) throws SchedulerException;
	public void deletar(Tarefa tarefa) throws SchedulerException;
	public void changeStatus(Tarefa tarefa) throws SchedulerException;

}
