package br.com.agendador.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agendador.app.model.entity.Tarefa;
import br.com.agendador.repository.ListQueryDslPredicateExecutor;

public interface TarefaRepository extends JpaRepository<Tarefa, Long>, ListQueryDslPredicateExecutor<Tarefa>, TarefaRepositoryCustom {

	
}
