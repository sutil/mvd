

@TypeDefs({
	@TypeDef(name = "localDate", defaultForType = LocalDate.class, typeClass= LocalDateUserType.class),
	@TypeDef(name = "momentoExecucao", defaultForType = MomentoExecucao.class, typeClass=MomentoExecucaoUserType.class),
	@TypeDef(name = "arquivo", defaultForType = Arquivo.class, typeClass=ArquivoUserType.class)
})

package br.com.agendador.app.userType;

import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.joda.time.LocalDate;

import br.com.agendador.app.model.MomentoExecucao;
import br.com.agendador.app.model.entity.Arquivo;


