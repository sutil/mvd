package br.com.agendador.app.userType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;

import br.com.agendador.app.model.entity.Arquivo;

public class ArquivoUserType extends ImmutableUserType {

	private static final long serialVersionUID = 1L;

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
			throws HibernateException, SQLException {
		String valor = rs.getString(names[0]);
		if(rs.wasNull()){
			return null;
		}
		return Arquivo.newInstance(valor);
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index)
			throws HibernateException, SQLException {
		if(value != null){
			Arquivo arq = (Arquivo) value;
			st.setString(index, arq.toString());
		}
		else{
			st.setNull(index, Types.VARCHAR);
		}
		
	}

	@Override
	public Class<?> returnedClass() {
		return Arquivo.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[]{Types.VARCHAR};
	}

}
