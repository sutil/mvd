package br.com.agendador.app.userType;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;

import br.com.agendador.app.model.MomentoExecucao;

public class MomentoExecucaoUserType extends ImmutableUserType {

	private static final long serialVersionUID = 1L;

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, Object arg2)
			throws HibernateException, SQLException {
		if(rs.wasNull()){
			return null;
		}
		return MomentoExecucao.fromString(rs.getString(names[0]));
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index)
			throws HibernateException, SQLException {
		if(value != null){
			MomentoExecucao m = (MomentoExecucao) value;
			st.setString(index, m.getCronString());
		}
		else{
			st.setNull(index, Types.VARCHAR);
		}
		
	}

	@Override
	public Class<?> returnedClass() {
		return MomentoExecucao.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[]{Types.VARCHAR};
	}

}
