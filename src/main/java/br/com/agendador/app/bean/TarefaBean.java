package br.com.agendador.app.bean;

import java.io.Serializable;

import br.com.agendador.app.model.Hora;
import br.com.agendador.app.model.Minuto;
import br.com.agendador.app.model.MomentoExecucao;
import br.com.agendador.app.model.entity.Arquivo;
import br.com.agendador.app.model.entity.Tarefa;
import br.com.agendador.app.model.enums.DiaSemana;
import br.com.agendador.app.model.enums.Status;

public class TarefaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private String descricao;
	private Integer hora;
	private Integer minuto;
	private DiaSemana diaSemana;
	private Arquivo arquivoSelecionado;
	private Status status = Status.INATIVA;
	
	public TarefaBean() {
	}
	
	public TarefaBean(Tarefa t){
		this.id = t.getId();
		this.descricao = t.getDescricao();
		this.hora = t.getMomentoExecucao().getHora().getHora();
		this.minuto = t.getMomentoExecucao().getMinuto().getminuto();
		this.diaSemana = t.getMomentoExecucao().getDiaSemana();
		this.arquivoSelecionado = t.getArquivo();
		this.status = t.getStatus();
	}
	
	
	
	public MomentoExecucao getMomentoExecucao(){
		return new MomentoExecucao(diaSemana, Hora.fromInteger(hora), Minuto.fromInteger(minuto));
	}
	
	
	public boolean isAtiva(){
		return status == Status.ATIVA;
	}
	
	public void setAtiva(boolean status){
		if(status){
			this.status = Status.ATIVA;
		}
		else{
			this.status = Status.INATIVA;
		}
	}
	
	public Status getStatus() {
		return status;
	}
	
	public String getDescricao() {
		return descricao;
	}

	public Long getId() {
		return id;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getHora() {
		return hora;
	}

	public void setHora(Integer hora) {
		this.hora = hora;
	}

	public Integer getMinuto() {
		return minuto;
	}

	public void setMinuto(Integer minuto) {
		this.minuto = minuto;
	}

	public DiaSemana getDiaSemana() {
		return diaSemana;
	}

	public void setDiaSemana(DiaSemana diaSemana) {
		this.diaSemana = diaSemana;
	}
	
	public Arquivo getArquivoSelecionado() {
		return arquivoSelecionado;
	}
	
	public void setArquivoSelecionado(Arquivo arquivoSelecionado) {
		this.arquivoSelecionado = arquivoSelecionado;
	}

}
