package br.com.agendador.app.login.seguranca.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agendador.app.login.seguranca.Usuario;
import br.com.agendador.repository.ListQueryDslPredicateExecutor;

public interface UsuarioRepository extends JpaRepository<Usuario , Long>, ListQueryDslPredicateExecutor<Usuario> {

}
