package br.com.agendador.app.controller;

import org.springframework.stereotype.Controller;

@Controller
public class QuemSomosController {
	
	public String getTitulo(){
		return "Título";
	}
	
	public String getTexto(){
		return "Quem Somos...";
	}

}