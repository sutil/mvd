package br.com.agendador.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;

import br.com.agendador.app.model.entity.Album;
import br.com.agendador.app.model.entity.Imagem;

@Controller
public class FotoController {

	public List<Album> getAlbuns(){
		ArrayList<Album> list = new ArrayList<Album>();
		list.add(new Album(new Imagem("images/capa-assessoria.jpg"), null));
		list.add(new Album(new Imagem("images/capa-fotos.jpg"), null));
		list.add(new Album(new Imagem("images/capa-contatos.jpg"), null));
		list.add(new Album(new Imagem("images/capa-quem-somos.jpg"), null));
		list.add(new Album(new Imagem("images/capa-contatos.jpg"), null));
		return list;
	}
	
	public List<Imagem> getImagens(Album album){
		ArrayList<Imagem> list = new ArrayList<Imagem>();
		list.add(new Imagem("images/capa-assessoria.jpg"));
		list.add(new Imagem("images/capa-fotos.jpg"));
		list.add(new Imagem("images/capa-contatos.jpg"));
		list.add(new Imagem("images/capa-quem-somos.jpg"));
		list.add(new Imagem("images/capa-contatos.jpg"));
		list.add(new Imagem("images/logo-capa.png"));
		return list;
	}
}
