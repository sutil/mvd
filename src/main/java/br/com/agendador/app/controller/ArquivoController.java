package br.com.agendador.app.controller;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Controller;

@Controller
@ManagedBean
public class ArquivoController {

	public void handleFileUpload(FileUploadEvent event) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		try {
			InputStream is = event.getFile().getInputstream();
			String path = ctx.getExternalContext().getRealPath("1"+getTipo(event.getFile()));
				
			System.out.println(path);
			
			File destino = new File(path);
			FileUtils.copyInputStreamToFile(is, destino);
			
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso no upload", "arquivo salvo."));
		} catch (IOException e) {
			ctx.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "falha ao salvar o arquivo"));
			e.printStackTrace();
		}
	}
	
	
	
	private String getTipo(UploadedFile file){
		String name = file.getFileName();
		return name.substring(name.lastIndexOf('.'), name.length());
	}

}
