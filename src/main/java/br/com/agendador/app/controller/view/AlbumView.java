package br.com.agendador.app.controller.view;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import br.com.agendador.app.model.entity.Album;
import br.com.agendador.app.model.entity.Imagem;

@ManagedBean
@ViewScoped
public class AlbumView implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Album album;
	private boolean inicializado;
	private List<Album> albuns;
	private List<String> imagensDoAlbum;
	
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	public Album getAlbum() {
		return album;
	}

	public void inicializa(List<Album> albuns) {
		this.albuns = albuns;
		this.inicializado = true;
	}

	public boolean isInicializado() {
		return inicializado;
	}
	
	public List<Album> getAlbuns() {
		return albuns;
	}

	public void addImagemNoAlbum(String imagem) {
		imagensDoAlbum.add(imagem);
	}

	public Imagem getCapa() {
		if(album == null)
			return null;
		
		return album.getCapa();
	}
	
	public void setCapa(Imagem capa) {
		this.album.setCapa(capa);
	}
}
