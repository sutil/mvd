package br.com.agendador.app.controller;

import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_INFO;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.event.FileUploadEvent;
import org.springframework.transaction.annotation.Transactional;

import br.com.agendador.app.controller.view.AlbumView;
import br.com.agendador.app.model.entity.Album;
import br.com.agendador.app.model.entity.Imagem;
import br.com.agendador.app.repository.AlbumRepository;
import br.com.agendador.app.repository.ImagemRepositoryImpl;

@ManagedBean
public class AlbumController {
	
	@ManagedProperty("#{albumView}")
	private AlbumView albumView;
	
	@Inject
	private AlbumRepository albumRepository;
	
	@Inject
	private ImagemRepositoryImpl imagemRepository;
	
	@PostConstruct
	public void init(){
		if(albumView.isInicializado()){
			albumView.inicializa(albumRepository.findAll());
		}
	}
	
	public AlbumView getAlbumView() {
		return albumView;
	}
	
	public void setAlbumView(AlbumView albumView) {
		this.albumView = albumView;
	}
	
	public void novo(){
		albumView.setAlbum(new Album());
	}
	
	public List<Album> getAlbuns(){
		return albumView.getAlbuns();
	}
	
	public void handleFileUpload(FileUploadEvent event) {
		
	}
	
	@Transactional	
	public void handleCapaUpload(FileUploadEvent event) {
		try {
			Imagem imagem = new Imagem(event.getFile());
			Imagem saved = imagemRepository.salvar(imagem, getFolder());
			
			albumView.setCapa(saved);
			
			message(SEVERITY_INFO, "Sucesso no upload");
		} catch (IOException e) {
			message(SEVERITY_ERROR, "Falha ao enviar arquivo");
			e.printStackTrace();
		}
	}

	private String getFolder(){
		return getContext().getExternalContext().getRealPath("fotos");
	}	
	
	private FacesContext getContext(){
		return FacesContext.getCurrentInstance();
	}
	
	private void message(Severity severity, String message){
		getContext().addMessage(null, new FacesMessage(severity, message, ""));
	}

}
