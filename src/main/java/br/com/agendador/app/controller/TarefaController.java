package br.com.agendador.app.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import br.com.agendador.app.bean.TarefaBean;
import br.com.agendador.app.model.entity.Tarefa;
import br.com.agendador.app.model.enums.DiaSemana;
import br.com.agendador.app.repository.TarefaRepository;

@Controller
public class TarefaController {
	
	@Autowired
	private TarefaRepository tarefaRepository;
	
	public List<Tarefa> getTarefas(){
		return tarefaRepository.findAll();
	}
	
	public TarefaBean novo(){
		return new TarefaBean();
	}
	
	public TarefaBean editar(Tarefa tarefa){
		return new TarefaBean(tarefa);
	}
	
	public void deletar(Tarefa tarefa){
		try{
			tarefaRepository.deletar(tarefa);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Sucesso!", "A tarefa "+tarefa.getDescricao()+" foi excluída."));
		}
		catch (Exception e){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro.", e.getMessage()));
		}
	}
	
	public List<DiaSemana> getDiasSemana(){
		return new ArrayList<DiaSemana>(Arrays.asList(DiaSemana.values()));
	}
	
	public void salvarTarefa(TarefaBean bean){
		try{
			Tarefa tarefa = new Tarefa(bean);
			tarefaRepository.salvar(tarefa);
			String msg = bean.getDescricao()+" salva com sucesso";
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", msg));
		}
		catch(Exception e){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage()));
		}
	}
	
	@Transactional
	public void changeStatus(Tarefa tarefa){
		try{
			tarefaRepository.changeStatus(tarefa);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Status da tarefa foi alterado.", tarefa.getDescricao()+" está "+tarefa.getStatus()));
		}
		catch(SchedulerException e){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage()));
		}
	}

}
