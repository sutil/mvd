package br.com.agendador.app.controller;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.stereotype.Controller;

@Controller
public class ContatoController {
	
	public MapModel getLocal(){
		DefaultMapModel model = new DefaultMapModel();
		
		LatLng coordenadas = new LatLng(-23.421196,-51.948555);
		model.addOverlay(new Marker(coordenadas, "MV - Festas"));
		return model;
	}

}
