package br.com.agendador.app.model.quartz;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import br.com.agendador.app.model.entity.Tarefa;
import br.com.agendador.app.repository.TarefaRepositoryCustom;

public class AgendadorDeTarefa {
	
	private static Scheduler getScheduler() throws SchedulerException{
		SchedulerFactory schedFact = new StdSchedulerFactory();
		Scheduler scheduler = schedFact.getScheduler();
		scheduler.start();
		return scheduler;
	}
	
	public static void agendar(Tarefa tarefa, TarefaRepositoryCustom tarefaRepositoryCustom) throws SchedulerException{
		JobDetail job = newJob(JobExecutor.class).build();
		
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put(JobExecutor.TAREFA, tarefa);
		jobDataMap.put(JobExecutor.TAREFA_REPOSITORY, tarefaRepositoryCustom);
		
		Trigger trigger = newTrigger().withIdentity(tarefa.getId().toString(),JobExecutor.TAREFA)
				.usingJobData(jobDataMap)
				.withSchedule(CronScheduleBuilder.cronSchedule(tarefa.getMomentoExecucao().getCronString()))
				.build();
		getScheduler().scheduleJob(job, trigger);
		
	}
	
	public static void cancelar(Tarefa tarefa) throws SchedulerException{
		getScheduler().unscheduleJob(new TriggerKey(tarefa.getId().toString(), JobExecutor.TAREFA));
	}

}
