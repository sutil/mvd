package br.com.agendador.app.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.base.Objects;

public class Hora implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private final Integer hora;
	
	private Hora(Integer hora) {
		this.hora = hora;
	}
	
	public static Hora fromInteger(Integer hora){
		checkNotNull(hora, "Hora não foi informada");
		checkArgument(hora >= 0 && hora <= 23, "Hora inválida");
		return new Hora(hora);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Hora){
			Hora other = (Hora) obj;
			return Objects.equal(this.hora, other.hora);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(hora);
	}
	
	public Integer getHora() {
		return hora;
	}

}
