package br.com.agendador.app.model.quartz;

import java.io.IOException;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import br.com.agendador.app.model.entity.Tarefa;
import br.com.agendador.app.repository.TarefaRepositoryImpl;

public class JobExecutor extends QuartzJobBean {
	
	public static final String TAREFA = "TAREFA";
	public static final String TAREFA_REPOSITORY = "TAREFA_REPOSITORY";

	@Override
	protected void executeInternal(JobExecutionContext ctx)
			throws JobExecutionException {		
		Tarefa tarefa = (Tarefa) ctx.getTrigger().getJobDataMap().get(TAREFA);
		TarefaRepositoryImpl repository = (TarefaRepositoryImpl) ctx.getTrigger().getJobDataMap().get(TAREFA_REPOSITORY);
		try {
			System.out.println("executando");
			tarefa.executar();
			System.out.println("historicos: "+tarefa.getHistoricos().size());
			Tarefa saved = repository.salvar(tarefa);
			System.out.println("historicos salvos: "+saved.getHistoricos().size());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
		
	}
	
	

	
	

}
