package br.com.agendador.app.model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Album implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	private Imagem capa;
	private String name;
	
	
	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Imagem> imagens;
	
	public Album() {}
	
	public Album(Imagem capa, List<Imagem> imagens){
		this.capa = capa;
		this.imagens = imagens;
	}
	
	public Long getId() {
		return id;
	}
	
	public Imagem getCapa() {
		return capa;
	}
	
	public void setCapa(Imagem capa) {
		this.capa = capa;
	}
	
	public List<Imagem> getImagens() {
		return imagens;
	}
	
	public String getName() {
		return name;
	}

}
