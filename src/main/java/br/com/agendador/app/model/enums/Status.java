package br.com.agendador.app.model.enums;


public enum Status {
	
	ATIVA {
		@Override
		public Status change() {
			return INATIVA;
		}
	}, 
	INATIVA {
		@Override
		public Status change() {
			return ATIVA;
		}
	};

	public abstract Status change();

}
