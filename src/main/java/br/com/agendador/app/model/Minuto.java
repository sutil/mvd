package br.com.agendador.app.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import com.google.common.base.Objects;

public class Minuto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private final Integer minuto;
	
	private Minuto(Integer minuto) {
		this.minuto = minuto;
	}
	
	public static Minuto fromInteger(Integer minuto){
		checkNotNull(minuto, "minuto não foi informada");
		checkArgument(minuto >= 0 && minuto <= 59, "minuto inválida");
		return new Minuto(minuto);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Minuto){
			Minuto other = (Minuto) obj;
			return Objects.equal(this.minuto, other.minuto);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(minuto);
	}
	
	public Integer getminuto() {
		return minuto;
	}

}
