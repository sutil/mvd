package br.com.agendador.app.model.enums;

import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public enum DiaSemana {
	
	TODOS("*", "Todos"), 
	DOMINGO("1", "Domingo"), 
	SEGUNDA("2", "Segunda-feira"), 
	TERCA("3", "Terça-feira"), 
	QUARTA("4", "Quarta-feira"), 
	QUINTA("5", "Quinta-feira"), 
	SEXTA("6", "Sexta-feira"), 
	SABADO("7", "Sábado");
	
	private final String value;
	private final String descricao;
	
	private DiaSemana(String value, String descricao) {
		this.value = value;
		this.descricao = descricao;
	}
	
	private static Map<String, DiaSemana> map;
	static{
		Builder<String, DiaSemana> builder = ImmutableMap.builder();
		for(DiaSemana d : values()){
			builder.put(d.value, d);
		}
		map = builder.build();
	}
	
	public static DiaSemana fromValue(String value){
		return map.get(value);
	}
	
	public String getValue() {
		return value;
	}
	
	public String getDescricao() {
		return descricao;
	}
	

}
