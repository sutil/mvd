package br.com.agendador.app.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

@Entity
public class Historico extends Entidade{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String log;
	
	@ManyToOne
	@JoinColumn(name = "tarefa_fk")
	private Tarefa tarefa;
	
	public Historico(){}
	
	private Historico(String log, Tarefa tarefa) {
		this.log = log;
		this.tarefa = tarefa;
	}
	
	public static Historico newInstance(String log, Tarefa tarefa){
		if(Strings.isNullOrEmpty(log)){
			log = "sucesso!";
		}
		return new Historico(log, tarefa);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Historico){
			Historico other = (Historico) obj;
			return Objects.equal(this.dataCriacao, other.dataCriacao) &&
					Objects.equal(this.log, other.log);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(dataCriacao, log);
	}
	
	public Long getId() {
		return id;
	}
	
	public String getLog() {
		return log;
	}
}
