package br.com.agendador.app.model.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.primefaces.model.UploadedFile;

@Entity
public class Imagem implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private Long id;
	
	private String url;
	
	@Transient
	private UploadedFile file;
	
	Imagem(){}
	
	public Imagem(UploadedFile file){
		this.file = file;
	}
	
	public Imagem(String url){
		this.url = url;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}	
	
	public UploadedFile getFile() {
		return file;
	}
}
