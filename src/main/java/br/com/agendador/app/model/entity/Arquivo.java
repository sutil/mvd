package br.com.agendador.app.model.entity;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Objects;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

public class Arquivo implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String nome;
	private final String path;

	private Arquivo(String nome, String path) {
		this.nome = nome;
		this.path = path;
	}

	public static Arquivo newInstance(String nome, String path) {
		checkArgument(!Strings.isNullOrEmpty(path), "Caminho do arquivo não encontrado no servidor");
		checkNotNull(nome, "Relacione este arquivo a uma tarefa.");
		return new Arquivo(nome, path);
	}
	
	public static Arquivo newInstance(String value){
		checkArgument(!Strings.isNullOrEmpty(value), "Dados do arquivo não podem ser nulos.");
		checkArgument(value.contains("|"), "A string que instancia o arquivo deve conter '|'");
		String[] split = value.split("\\|");
		return new Arquivo(split[0], split[1]);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Arquivo) {
			Arquivo other = (Arquivo) obj;
			return Objects.equal(this.path, other.path);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(path);
	}
	
	@Override
	public String toString() {
		return nome+"|"+path;
	}
	
	public String execute() throws IOException{
		ProcessBuilder builder = new ProcessBuilder(path);
		Process process = builder.start();
		
		InputStream errorStream = process.getErrorStream();
		InputStream inputStream = process.getInputStream();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		StringBuilder sb = new StringBuilder();
		
		while((line = reader.readLine()) != null){
			sb.append(line);
		}
		
		BufferedReader readerError = new BufferedReader(new InputStreamReader(errorStream));
		sb.append("\n");
		line = null;
		while((line = readerError.readLine()) != null){
			sb.append(line);
		}
		
		return sb.toString();
	}

	public static String getBasePathSystem() {
		String osName = System.getProperty("os.name");
		String basePath;
		String path = System.getProperty("user.dir");
		if (osName.toLowerCase().contains("win")) {
			Pattern regex = Pattern.compile("(.*?\\/)");
			Matcher m = regex.matcher(path);
			if (m.find()) {
				basePath = m.group();
			}
			basePath = "C:" + File.separator + "moomac" + File.separator
					+ "integracao";
		} else {
			basePath = path + File.separator + "moomac" + File.separator
					+ "integracao";
		}
		return basePath;
	}

	public static List<Arquivo> getFilesJobs() {
		File folder = new File(getBasePathSystem());
		List<Arquivo> names = Lists.newArrayList();
		if (folder != null && folder.exists()) {
			filesNames(folder, names);
		}
		return names;
	}

	private static void filesNames(File file, List<Arquivo> names) {
		String name = file.getName();
		if (name.contains(".bat") || name.contains(".sh")) {
			names.add(newInstance(name, file.getAbsolutePath()));
		}
		File[] children = file.listFiles();
		if (children != null)
			for (File child : children) {
				filesNames(child, names);
			}
	}

	public String getNome() {
		return nome;
	}

	public String getPath() {
		return path;
	}

}
