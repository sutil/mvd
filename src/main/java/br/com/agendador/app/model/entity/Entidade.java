package br.com.agendador.app.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class Entidade implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_criacao")
	protected Date dataCriacao;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_atualizacao")
	protected Date dataAtualizacao;

	public Date getDataCriacao() {
		return dataCriacao;
	}

	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}
	
	@PrePersist
	public void prepersiste(){
		this.dataCriacao = new Date();
		this.dataAtualizacao = new Date();
	}
	
	@PreUpdate
	public void preupdate(){
		this.dataAtualizacao = new Date();
	}

}
