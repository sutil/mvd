package br.com.agendador.app.model.arquivo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import br.com.agendador.app.model.entity.Arquivo;

public class Unzip {

	List<String> fileList;
	private final String OUTPUT_FOLDER;
	private static final String separator = File.separator;

	public Unzip() {
		this.OUTPUT_FOLDER = Arquivo.getBasePathSystem();
	}

	

	public void unZipIt(InputStream is) throws IOException {

		byte[] buffer = new byte[1024];

		File folder = new File(OUTPUT_FOLDER);
		if (!folder.exists()) {
			folder.mkdir();
		}

		ZipInputStream zis = new ZipInputStream(is);
		ZipEntry ze = zis.getNextEntry();

		while (ze != null) {

			String fileName = ze.getName();
			File newFile = new File(OUTPUT_FOLDER + separator + fileName);

			new File(newFile.getParent()).mkdirs();
			FileOutputStream fos = new FileOutputStream(newFile);

			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}

			fos.close();
			ze = zis.getNextEntry();
		}

		zis.closeEntry();
		zis.close();

	}

}
