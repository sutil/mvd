package br.com.agendador.app.model;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.Serializable;
import java.text.DecimalFormat;

import com.google.common.base.Objects;
import com.google.common.base.Strings;

import br.com.agendador.app.model.enums.DiaSemana;

public class MomentoExecucao implements Serializable {

	private static final long serialVersionUID = 1L;

	private final DiaSemana diaSemana;
	private final Hora hora;
	private final Minuto minuto;

	public MomentoExecucao(DiaSemana diaSemana, Hora hora, Minuto minuto) {
		this.diaSemana = diaSemana;
		this.hora = hora;
		this.minuto = minuto;
	}
	
	public static MomentoExecucao fromString(String cronExpression){
		checkArgument(!Strings.isNullOrEmpty(cronExpression), "cronExpression não definido");
		String[] split = cronExpression.split(" ");
		checkArgument(split.length == 7, "Parâmetros do cronExpression estão faltando. ["+cronExpression+"] não contém 7 parâmetros");
		
		DiaSemana diaSemana = DiaSemana.fromValue(split[5]);
		Hora hora = Hora.fromInteger(Integer.valueOf(split[2]));
		Minuto minuto = Minuto.fromInteger(Integer.valueOf(split[1]));
		return new MomentoExecucao(diaSemana, hora, minuto);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof MomentoExecucao){
			MomentoExecucao other = (MomentoExecucao) obj;
			return Objects.equal(this.diaSemana, other.diaSemana) &&
					Objects.equal(this.hora, other.hora) &&
					Objects.equal(this.minuto, other.minuto);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(diaSemana, hora, minuto);
	}
	
	
	@Override
	public String toString() {
		DecimalFormat df = new DecimalFormat("00");
		return String.format("%s - %s:%s", diaSemana.getDescricao(), df.format(hora.getHora()), df.format(minuto.getminuto()));
	}
	
	/**
	 * See documentation in http://quartz-scheduler.org/documentation/quartz-1.x/tutorials/crontrigger
	 * @return cronExpression for quartz scheduler.
	 */
	public String getCronString(){
		return String.format("0 %d %d ? * %s *", minuto.getminuto(), hora.getHora(), diaSemana.getValue());
	}
	
	public DiaSemana getDiaSemana() {
		return diaSemana;
	}
	
	public Hora getHora() {
		return hora;
	}
	
	public Minuto getMinuto() {
		return minuto;
	}

}
