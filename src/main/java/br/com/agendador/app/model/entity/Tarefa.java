package br.com.agendador.app.model.entity;

import java.io.IOException;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;
import org.quartz.SchedulerException;

import br.com.agendador.app.bean.TarefaBean;
import br.com.agendador.app.model.MomentoExecucao;
import br.com.agendador.app.model.enums.Status;
import br.com.agendador.app.model.quartz.AgendadorDeTarefa;
import br.com.agendador.app.repository.TarefaRepositoryCustom;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

@Entity
public class Tarefa extends Entidade {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String descricao;
	
	@Type(type = "momentoExecucao")
	private MomentoExecucao momentoExecucao;
	
	@Type(type = "arquivo")
	private Arquivo arquivo;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status = Status.INATIVA;
	
	@OneToMany(mappedBy = "tarefa", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Historico> historicos = Lists.newArrayList();
	
	Tarefa(){}
	
	public Tarefa(TarefaBean bean){
		this.id = bean.getId();
		this.arquivo = bean.getArquivoSelecionado();
		this.descricao = bean.getDescricao();
		this.momentoExecucao = bean.getMomentoExecucao();
		this.status = bean.getStatus();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Tarefa){
			Tarefa other = (Tarefa) obj;
			return Objects.equal(this.descricao, other.descricao) &&
				   Objects.equal(this.momentoExecucao, other.momentoExecucao);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(descricao, momentoExecucao);
	}
	
	public void executar() throws IOException{
		String log = arquivo.execute();
		System.out.println("log > >: "+log+"\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		historicos.add(Historico.newInstance(log, this));
	}
	
	
	public List<Historico> getHistoricos() {
		return historicos;
	}
	
	
	
	public Long getId() {
		return id;
	}
	
	public void changeStatus() throws SchedulerException{
		this.status =  this.status.change();
	}
	
	public Status getStatus() {
		return status;
	}
	
	public void unScheduler() throws SchedulerException{
		AgendadorDeTarefa.cancelar(this);
		this.status = Status.INATIVA;
	}
	
	public void scheduler(TarefaRepositoryCustom tarefaRepositoryCustom) throws SchedulerException{
		AgendadorDeTarefa.agendar(this, tarefaRepositoryCustom);
		this.status = Status.ATIVA;
	}
	
	public boolean isAtiva(){
		return status == Status.ATIVA;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public MomentoExecucao getMomentoExecucao() {
		return momentoExecucao;
	}

	public void setMomentoExecucao(MomentoExecucao momentoExecucao) {
		this.momentoExecucao = momentoExecucao;
	}
	
	public Arquivo getArquivo() {
		return arquivo;
	}
	
	

}
